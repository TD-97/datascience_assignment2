# Text Classification
The task of this project was to scrape a website for reviews and ratings across three different categories of business. Then use a logisitical regression classifier which would be able to predict whether a review was positive or negative based on the text of the review. Using the star-rating of the review to ascertain the result.

The accuracy of the classifier is found to be around 90%, in the notebook the mistakes of the classifier are examined to see what type of review tends to trip it up.